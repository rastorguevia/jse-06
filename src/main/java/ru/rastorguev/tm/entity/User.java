package ru.rastorguev.tm.entity;

import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

public class User extends AbstractEntity {
    private String login;
    private String passHash;
    private Role role;

    public User() {
    }

    public User(Role role) {
        this.role = role;
    }

    public User(String login, String passHash, Role role) {
        this.login = login;
        this.passHash = mdHashCode(passHash);
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = mdHashCode(passHash);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
