package ru.rastorguev.tm.entity;

import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;

public class Project extends AbstractEntity {
    private String userId;
    private String name = "";
    private String description = "";
    private Date startDate = stringToDate(dateFormatter.format(new Date()));
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}