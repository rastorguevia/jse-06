package ru.rastorguev.tm.view;

import static ru.rastorguev.tm.util.DateUtil.*;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class View {

    public View() {
    }

    public static void showWelcomeMsg() {
        System.out.println("* TASK MANAGER * \n" +
                "* help - show all commands * \n" +
                "* command case is not important *");
    }

    public static void showUnknownCommandMsg() {
        System.out.println("Unknown Command \n" +
                "try again");
    }

    public static void showAccessDeniedMsg() {
        System.out.println("Access Denied");
    }

    public static void printAllProjects(Collection<Project> projectCollection) {
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectCollection);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public static void printAllProjectsForUser(List<Project> listOfProjects) {
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public static void printProject(Project project) {
        System.out.println("Project name: " + project.getName() +
                "\nProject description: " + project.getDescription() +
                "\nProject start date: " + dateFormatter.format(project.getStartDate()) +
                "\nProject end date: " + dateFormatter.format(project.getEndDate()));
    }

    public static void printTaskListByProjectId(String projectId, Collection<Task> taskCollection) {
        List<Task> listOfTasks = new LinkedList<>();
        List<Task> filteredListOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskCollection);
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (listOfTasks.get(i).getProjectId().equals(projectId)) {
                filteredListOfTasks.add(listOfTasks.get(i));
            }
        }
        for (int i = 0; i < filteredListOfTasks.size(); i++) {
            System.out.println((i + 1) + "." + filteredListOfTasks.get(i).getName());
        }
    }

    public static void printTask(Task task) {
        System.out.println("Task name: " + task.getName() +
                "\nTask description: " + task.getDescription() +
                "\nTask start date: " + dateFormatter.format(task.getStartDate()) +
                "\nTask end date: " + dateFormatter.format(task.getEndDate()));
    }

    public static void printUserProfile(User user) {
        System.out.println("User login: " + user.getLogin() +
                "\nUser role: " + user.getRole().getDisplayName());
    }
}