package ru.rastorguev.tm;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}