package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.view.View.*;

import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.command.system.ExitCommand;
import ru.rastorguev.tm.command.system.HelpCommand;
import ru.rastorguev.tm.command.project.*;
import ru.rastorguev.tm.command.task.*;
import ru.rastorguev.tm.command.user.*;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.service.ProjectService;
import ru.rastorguev.tm.service.TaskService;
import ru.rastorguev.tm.service.UserService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectRepository projectRepository = new ProjectRepository(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    public Map<String, AbstractCommand> getCommands() {
        return commandMap;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void init() throws Exception {
        commandRegistry(new HelpCommand(this));
        commandRegistry(new ProjectClearCommand(this));
        commandRegistry(new ProjectCreateCommand(this));
        commandRegistry(new ProjectEditCommand(this));
        commandRegistry(new ProjectListCommand(this));
        commandRegistry(new ProjectRemoveCommand(this));
        commandRegistry(new ProjectSelectCommand(this));
        commandRegistry(new TaskClearCommand(this));
        commandRegistry(new TaskCreateCommand(this));
        commandRegistry(new TaskEditCommand(this));
        commandRegistry(new TaskListCommand(this));
        commandRegistry(new TaskRemoveCommand(this));
        commandRegistry(new TaskSelectCommand(this));
        commandRegistry(new UserLogInCommand(this));
        commandRegistry(new UserLogOutCommand(this));
        commandRegistry(new UserUserRegistryCommand(this));
        commandRegistry(new UserAdminRegistryCommand(this));
        commandRegistry(new UserPasswordUpdateCommand(this));
        commandRegistry(new UserCheckAndEditProfileCommand(this));
        commandRegistry(new ExitCommand(this));

        createUsers();

        launchConsole();
    }

    private void commandRegistry(AbstractCommand command) {
        commandMap.put(command.getName(), command);
    }

    public void launchConsole() throws Exception {
        showWelcomeMsg();

        String input = "";
        while (true) {
            input = reader.readLine().toLowerCase();
            execute(input);
        }
    }

    private void execute(final String input) throws Exception {
        if (input == null || input.isEmpty()) {
            showUnknownCommandMsg();
            return;
        }
        final AbstractCommand command = commandMap.get(input);
        if (command == null) {
            showUnknownCommandMsg();
            return;
        }
        final boolean secureCheck = !command.secure() || (command.secure() && userService.isAuth());
        final boolean roleCheck = command.roles() == null ||
                (command.roles() != null && userService.isRolesAllowed(command.roles()));
        if (secureCheck && roleCheck) command.execute();
        else showAccessDeniedMsg();
    }

    private void createUsers() {
        userService.userRegistry("user", "user123");
        userService.adminRegistry("admin", "admin123");
    }
}