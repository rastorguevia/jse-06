package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.error.EntityDuplicateException;

import java.util.*;

public class ProjectRepository {
    private final Map<String, Project> projectMap = new LinkedHashMap<>();
    private final TaskRepository taskRepository;

    public ProjectRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Collection<Project> findAll() {
        return projectMap.values();
    }

    public Project findOne(String projectId) { return projectMap.get(projectId); }

    public Project persist(Project project) {
        if (projectMap.containsKey(project.getId())) throw new EntityDuplicateException("Project already exist");
        return projectMap.put(project.getId(), project);
    }

    public Project merge(Project project) {
        projectMap.put(project.getId(), project);
        return project;
    }

    public Project remove(String projectId) {
        List<Task> taskList = new LinkedList<>();
        taskList.addAll(taskRepository.findAll());
        for (Task task: taskList) {
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
            }
        }
        return projectMap.remove(projectId);
    }

    public void removeAll() {
        projectMap.clear();
        taskRepository.removeAll();
    }
}