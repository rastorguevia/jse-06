package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.error.EntityDuplicateException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {
    private final Map<String, User> userMap = new LinkedHashMap<>();

    public Collection<User> findAll() {
        return userMap.values();
    }

    public User findOne(String userId) {
        return userMap.get(userId);
    }

    public User persist(User user) {
        if (userMap.containsKey(user.getId())) throw new EntityDuplicateException("User ID already exist");
        return userMap.put(user.getId(), user);
    }

    public User merge(User user) {
        userMap.put(user.getId(), user);
        return user;

    }

    public User remove(String userId) {
        return userMap.remove(userId);
    }

    public void removeAll() {
        userMap.clear();
    }
}