package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.enumerated.Role;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract boolean secure();
    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute() throws IOException;
    public abstract Role[] roles();
}