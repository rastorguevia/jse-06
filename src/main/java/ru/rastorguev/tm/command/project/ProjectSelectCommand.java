package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class ProjectSelectCommand extends AbstractCommand {

    public ProjectSelectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_select";
    }

    @Override
    public String getDescription() {
        return "Select exact project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project select");
        System.out.println("Enter project ID");
        User user = bootstrap.getUserService().getCurrentUser();
        printAllProjectsForUser(bootstrap.getProjectService().findAllByUserId(user.getId()));
        String projectId = bootstrap.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(reader.readLine()), user.getId());
        Project project = bootstrap.getProjectService().findOne(projectId);
        printProject(project);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}