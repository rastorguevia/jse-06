package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project edit");
        System.out.println("Enter project ID");
        User user = bootstrap.getUserService().getCurrentUser();
        printAllProjectsForUser(bootstrap.getProjectService().findAllByUserId(user.getId()));
        String projectId = bootstrap.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(reader.readLine()), user.getId());
        Project project = bootstrap.getProjectService().findOne(projectId);
        Project editedProject = new Project();
        editedProject.setId(project.getId());
        System.out.println("Edit name? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter project name");
            editedProject.setName(reader.readLine());
        } else editedProject.setName(project.getName());
        System.out.println("Edit description? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedProject.setDescription(reader.readLine());
        } else editedProject.setDescription(project.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedProject.setStartDate(stringToDate(reader.readLine()));
        } else editedProject.setStartDate(project.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedProject.setEndDate(stringToDate(reader.readLine()));
        } else editedProject.setEndDate(project.getEndDate());
        bootstrap.getProjectService().merge(editedProject);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}