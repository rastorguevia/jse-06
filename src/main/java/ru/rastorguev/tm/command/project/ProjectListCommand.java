package ru.rastorguev.tm.command.project;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.view.View.*;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Project list");
        User user = bootstrap.getUserService().getCurrentUser();
        printAllProjectsForUser(bootstrap.getProjectService().findAllByUserId(user.getId()));
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}