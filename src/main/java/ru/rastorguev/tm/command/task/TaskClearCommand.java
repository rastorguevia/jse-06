package ru.rastorguev.tm.command.task;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "Remove project tasks.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Enter Project ID");
        User user = bootstrap.getUserService().getCurrentUser();
        printAllProjectsForUser(bootstrap.getProjectService().findAllByUserId(user.getId()));
        String projectId = bootstrap.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(reader.readLine()), user.getId());
        bootstrap.getTaskService().removeTaskListByProjectId(projectId);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}