package ru.rastorguev.tm.command.task;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;
import java.util.List;

import static ru.rastorguev.tm.view.View.*;

public class TaskSelectCommand extends AbstractCommand {

    public TaskSelectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "task_select";
    }

    @Override
    public String getDescription() { return "Select exact task."; }

    @Override
    public void execute() throws IOException {
        System.out.println("Task select");
        System.out.println("Enter Project ID");
        User user = bootstrap.getUserService().getCurrentUser();
        printAllProjectsForUser(bootstrap.getProjectService().findAllByUserId(user.getId()));
        String projectId = bootstrap.getProjectService().getProjectIdByNumberForUser(Integer.parseInt(reader.readLine()), user.getId());
        printTaskListByProjectId(projectId, bootstrap.getTaskService().findAll());
        List<Task> filteredTaskList = bootstrap.getTaskService().filterTaskListByProjectId(projectId, bootstrap.getTaskService().findAll());
        System.out.println("Enter Task ID");
        String taskId = bootstrap.getTaskService().getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        printTask(bootstrap.getTaskService().findOne(taskId));
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}