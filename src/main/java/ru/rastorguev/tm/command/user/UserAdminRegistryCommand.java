package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public class UserAdminRegistryCommand extends AbstractCommand {

    public UserAdminRegistryCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "admin_registry";
    }

    @Override
    public String getDescription() {
        return "Registry new Admin.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Admin registry");
        System.out.println("Enter login");
        String login = reader.readLine();
        System.out.println("Enter password");
        String password = reader.readLine();
        bootstrap.getUserService().adminRegistry(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}
