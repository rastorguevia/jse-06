package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public class UserLogOutCommand extends AbstractCommand {

    public UserLogOutCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "log_out";
    }

    @Override
    public String getDescription() {
        return "User log out.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Log out");
        bootstrap.getUserService().logOut();
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
