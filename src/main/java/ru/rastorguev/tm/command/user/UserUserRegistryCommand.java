package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public class UserUserRegistryCommand extends AbstractCommand {

    public UserUserRegistryCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "user_registry";
    }

    @Override
    public String getDescription() {
        return "Registry new User.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("User registry");
        System.out.println("Enter login");
        String login = reader.readLine();
        System.out.println("Enter password");
        String password = reader.readLine();
        bootstrap.getUserService().userRegistry(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return null;
    }
}
