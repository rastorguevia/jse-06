package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class UserCheckAndEditProfileCommand extends AbstractCommand {

    public UserCheckAndEditProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "check_edit";
    }

    @Override
    public String getDescription() {
        return "Check and edit current profile.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Check and edit");
        User user = bootstrap.getUserService().getCurrentUser();
        printUserProfile(user);
        System.out.println("Do you want to edit? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new login");
            String login = reader.readLine();
            if (bootstrap.getUserService().isExistByLogin(login)) {
                System.out.println("This login already exist");
            } else user.setLogin(login);
            bootstrap.getUserService().merge(user);
            System.out.println("OK");
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}