package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

public class UserPasswordUpdateCommand extends AbstractCommand {

    public UserPasswordUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getName() {
        return "password_update";
    }

    @Override
    public String getDescription() {
        return "Update password.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Password update");
        User user = bootstrap.getUserService().getCurrentUser();
        System.out.println("Enter new password");
        user.setPassHash(reader.readLine());
        bootstrap.getUserService().merge(user);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
