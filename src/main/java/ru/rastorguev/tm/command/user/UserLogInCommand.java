package ru.rastorguev.tm.command.user;

import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

public class UserLogInCommand extends AbstractCommand {

    public UserLogInCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getName() {
        return "log_in";
    }

    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Log in");
        System.out.println("Enter login");
        String login = reader.readLine();
        System.out.println("Enter password");
        String password = reader.readLine();
        bootstrap.getUserService().logIn(login, password);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return null;
    }
}
