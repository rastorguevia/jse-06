package ru.rastorguev.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

    public static String mdHashCode(String string) {
        if (string == null || string.isEmpty()) return null;
        byte[] digest = new byte[0];

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(string.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String hashResult = bigInt.toString(16);

        while( hashResult.length() < 32 ){
            hashResult = "0" + hashResult;
        }
        return hashResult;
    }
}