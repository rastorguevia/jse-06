package ru.rastorguev.tm.service;

import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.repository.UserRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

public class UserService {
    private UserRepository userRepository;
    private User currentUser = null;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.findOne(userId);
    }

    public User persist(User user) {
        if (user == null) return null;
        return userRepository.persist(user);
    }

    public User merge(User user) {
        if (user == null) return null;
        return userRepository.merge(user);
    }

    public User remove(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.remove(userId);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public void userRegistry(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        User user = new User(login, password, Role.USER);
        userRepository.persist(user);
    }

    public void adminRegistry(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        User admin = new User(login, password, Role.ADMINISTRATOR);
        userRepository.persist(admin);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(userRepository.findAll());
        for (User user : listOfUsers) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public boolean isExistByLogin(final String login) {
        if (login == null || login.isEmpty()) return false;
        List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(userRepository.findAll());
        for (User user : listOfUsers) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public boolean isRolesAllowed(Role... roles) {
        if (roles == null) return false;
        if (currentUser == null || currentUser.getRole() == null) return false;
        final List<Role> listOfRoles = Arrays.asList(roles);
        return listOfRoles.contains(currentUser.getRole());
    }

    public void logIn(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final User user = findByLogin(login);
        if (user == null) return;
        if (user.getPassHash().equals(mdHashCode(password))) {
            currentUser = user;
        }
    }

    public void logOut() {
        currentUser = null;
    }
}