package ru.rastorguev.tm.service;

import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectRepository.findAll());
        List<Project> filteredListOfProjects = new LinkedList<>();
        for (Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                filteredListOfProjects.add(project);
            }
        }
        return filteredListOfProjects;
    }

    public Project findOne(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOne(projectId);
    }

    public Project persist(Project project) {
        if (project == null) return null;
        return projectRepository.persist(project);
    }

    public Project merge(Project project) {
        if (project == null) return null;
        return projectRepository.merge(project);
    }

    public Project remove(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.remove(projectId);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public void removeAllByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectRepository.findAll());
        for (Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                projectRepository.remove(project.getId());
            }
        }
    }

    public String getProjectIdByNumber (int number) {
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectRepository.findAll());
        for (Project project : listOfProjects) {
            if (project.getId().equals(listOfProjects.get(number - 1).getId())){
                return project.getId();
            }
        }
        return null;
    }

    public String getProjectIdByNumberForUser (int number, final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Project> filteredListOfProjects = findAllByUserId(userId);
        for (Project project : filteredListOfProjects) {
            if (project.getId().equals(filteredListOfProjects.get(number - 1).getId())){
                return project.getId();
            }
        }
        return null;
    }
}